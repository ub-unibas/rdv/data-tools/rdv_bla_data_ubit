import os
from pkg_resources import Requirement, resource_filename

from rdv_data_helpers_ubit import get_rdv_logger
from rdv_marc_ubit import AlmaIZMarcJSONRecord, MarcTransformRule, HiSaJSONRecord
from cache_decorator_redis_ubit import CacheDecorator, NoCacheDecorator, CacheDecorator12h, CacheDecoratorEmtyValues
from mongo_decorator_ubit import create_mongo_decorator

from digi_oai_eportal_ubit.digi_oai_harvester import UBS_SLSP_OAI
from rdv_bla_data_ubit.bla_data import RDVBlaData

logger = get_rdv_logger("data_bla")

MongoDecorator = create_mongo_decorator(config_file=resource_filename(
    Requirement.parse("mongo_decorator_ubit"),
    "mongo_decorator_ubit/cfg/mongo_emptyvalues.yaml"), user=os.getenv("MONGO_USER"), password=os.getenv("MONGO_PWD"))


slsp_oai_store = MongoDecorator(db_name="slsp_oai_harvesting",
                                collection_name="bla")

slsp_oai = UBS_SLSP_OAI(oai_metadata="marc21",
                        oai_set="ubs_bla",
                        oai_sets={},
                        cachedecorator=CacheDecorator12h,
                        oai_store=slsp_oai_store)

marc_rule = MarcTransformRule(
    gd_service_file=os.getenv("GD_SERVICE_FILE"),
    gd_service_account_env_var=None,
    marc_spreadsheet="1_hOpdLwgabXUAJJkZyAWkmhTreDQY0vG8544ul_9QR0",
    marc_record_class=HiSaJSONRecord,
    gd_cache_decorator=NoCacheDecorator)

oai_harvester = RDVBlaData(oai_set="ubs_bla",
                                  oai_metadata="marc21",
                                  oai_sets={},
                                  cachedecorator=CacheDecorator12h(),
                                  entity_cachedecorator =CacheDecoratorEmtyValues(),
                                  oai_store=slsp_oai_store,
                                  gd_service_file=os.getenv("GD_SERVICE_FILE"),
                                  utils_mongo_store=MongoDecorator(db_name="digi_utils",
                                                                   collection_name="img_coords"),
                                  slsp_oai=slsp_oai,
                                  marc_rule=marc_rule,
                                  logger=logger,
                                  es_host=os.getenv("ES_HOST"))

oai_harvester.ingest2index(sub_index="bla_rdv",
                           index_prefix="bla",
                           xml_process_func=oai_harvester.process_rdvmarc_data,
                           incremental=False,
                           reharvest=False)