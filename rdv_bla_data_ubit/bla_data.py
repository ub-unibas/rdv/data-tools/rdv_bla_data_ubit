import os
import re
import requests
from copy import deepcopy

from bs4 import BeautifulSoup

from cache_decorator_redis_ubit import CacheDecorator, NoCacheDecorator, CacheDecorator12h, CacheDecoratorEmtyValues
from rdv_oaimarc_data_ubit import RDVOAIMarcData
from rdv_entity_data_ubit import RDVEntityData

class RDVBlaEntity(RDVEntityData):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.typo3_persons = self.get_person_entries()

    @property
    def link_field(self):
        """field which is used also in Object-Index to create Links"""
        return "adress_empf"

    @property
    def entity_type(self):
        return "AutorIn"

    def get_person_entries(self):

        @self.cachedecorator
        def cache_bla_typo3_entries():
            authors_dict = {}
            main_path = "/home/martin/PycharmProjects/rdv_indexbuilder/iiif_index_utils/data/bla"
            for file in os.listdir(main_path):
                if file not in ["index.txt", "geschichte.txt", "impressum.txt", "links.txt", "sammlungsprofil.txt"]:
                    file_data = open(os.path.join(main_path, file))
                    author = {}
                    soup = BeautifulSoup(file_data, 'html.parser')
                    for h1 in soup.find_all('h1'):
                        author.setdefault("name",[]).append(h1.text)
                    for p in soup.find_all('p'):
                        # teilweise durch <br/> getrennt
                        text_parts = p.find_all(text=True, recursive=False)
                        text = ". ".join([t for t in text_parts if t.strip(":")])
                        if p:
                            for i in p.find_all('i'):
                                header = i.text
                                header = header.strip(": ")
                                header_spez = ""
                                if " (" in header:
                                    header_parts = header.split(" (")
                                    header = header_parts[0]
                                    header_spez = header_parts[-1].replace(")","")
                        if not text:
                            author.setdefault("typo3_lebensdaten",[]).append(header)
                        else:
                            text = text.strip(": ")
                            if header_spez:
                                text ="{}: {}".format(header_spez, text)
                            author.setdefault("typo3_" + header, []).append(text)
                    authors_dict[file.split(".")[0]] = author
            return authors_dict
        return cache_bla_typo3_entries()


    @staticmethod
    def build_typo3_lookup_str(name):
        from unidecode import unidecode
        name = name.split(".")[0]
        name = name.replace("ä","ae")
        name = name.replace("ö", "oe")
        name = name.replace("ü", "ue")
        ascii_name = unidecode(name)
        name_parts = ascii_name.rsplit(", ", maxsplit=1)
        name_parts[0] = name_parts[0].replace(" ", "_")
        name_parts[0] = name_parts[0].replace("-", "_")
        typo3_key = "_".join([name_parts[0].lower(), name_parts[-1].lower()])
        return typo3_key

    def extend_pers_data(self, person, gnd_id):
        @CacheDecoratorEmtyValues()
        def check_typo3_img(typo3_key):
            return requests.get("https://ub2.unibas.ch/fileadmin/spezialkataloge/bla/{}.jpg".format(typo3_key))

        name = person.get("gnd_name") or person.get("gnd_name_id")[0].get("label")
        person["author"] = person.get("gnd_name_id")
        typo3_key = self.build_typo3_lookup_str(name)
        typo3_person = deepcopy(self.typo3_persons.get(typo3_key))

        if typo3_person:
            person.update(typo3_person)
            import json
            person["typo3_data_length"] = len(json.dumps(typo3_person))
            person["typo3_fields"] = list(typo3_person.keys())
            person.setdefault("old_bla_link", []).append({"label": "alter Link",
                                                          "link": "https://ub.unibas.ch/cmsdata/spezialkataloge/bla/{}.html".format(typo3_key)})
            person.setdefault("tag", []).append("typo3 Eintrag für AutorIn")
            person.setdefault("bla_entry", True)
            if check_typo3_img(typo3_key):
                person["thumbnail"] = {"source": "https://ub2.unibas.ch/fileadmin/spezialkataloge/bla/{}.jpg".format(typo3_key)}
                person.setdefault("tag", []).append("typo3 Bild")

class RDVBlaData(RDVOAIMarcData):
    index = "blardv"
    pers_index = "blapers"
    pers_index_prefix = "blapers"

    entity_class = RDVBlaEntity

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.object_entity_lookup = {}


    @CacheDecorator12h()
    def get_gbooks_data(self, isbn):
        if isbn and isbn[0].isdigit():
            isbn = isbn.replace("-","")
            isbn = re.sub(" .*","", isbn)
            url = "https://www.googleapis.com/books/v1/volumes"
            params = {"key": "AIzaSyCNT0EYhWSArjvSQmbD2EO4peHv2GODmlY", "q": "isbn:{}".format(isbn)}

            gb_entries = requests.get(url, params=params).json().get("items",[])
            previewLink = ""
            thumbnail = ""
            for gb_json in gb_entries:
                viewability  = gb_json.get("accessInfo",{}).get("viewability")

                if viewability == "PARTIAL" and gb_json.get("volumeInfo", {}).get("previewLink"):
                    previewLink = gb_json.get("volumeInfo", {}).get("previewLink")
                if gb_json.get("volumeInfo", {}).get("imageLinks", {}).get("thumbnail"):
                    thumbnail = gb_json.get("volumeInfo", {}).get("imageLinks", {}).get("thumbnail")
                    thumbnail.replace("http://", "https://")
            return thumbnail, previewLink
        else:
            return "", ""

    def extend_record_data(self, record, bsiz_id):
        record["type"] = []

        type_ = record.get("990_f",[])
        dok_about = record.get("990_x",[])
        isbn = record.get("020_z", [])

        # todo add dok_about to authors
        #264_b, 949_bjx

        authors_880_ad = []
        if record.get("880_ad"):
            for a in record.get("880_ad",[]):
                #startswith because of  100-01/(B https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9972503800905504&recordOwner=41SLSP_NETWORK
                if a.get("id","").startswith("(DE-588)") and a.get("role","").startswith("100-01"):
                    a["id"] = a["id"].replace("(DE-588)","https://d-nb.info/gnd/")
                    authors_880_ad.append(a)
        if authors_880_ad:
            record["author"]=authors_880_ad


        authors = record.get("author",[])
        persons = deepcopy(authors)

        if isbn and 0:
            print (isbn)
            record["thumbnail"], record["preview_link"] = self.get_gbooks_data(isbn[0])
            if record["thumbnail"]:
                record.setdefault("tag", []).append("Thumbnail")
            if record["preview_link"]:
                record.setdefault("tag", []).append("GooglePreview")

        if "blalit"in type_:
            record["type"].append("Weiterführende Literatur")
        elif "blaaut" in type_:
            if authors:
                record["type"].append("Werk")
            else:
                if dok_about:
                    record["type"].append("Dokumente über")
                else:
                    record["type"].append("Anthologie")

        for p in record.get("990_n", []):
            if p.startswith("NELA100"):
                year = "20{}".format(p[7:9])
                record.setdefault("neuerwerbungscode", []).append(year)
        #subfields=["B", "F", "b", "j", "x"]
        if 0:
            for p in record.get("949_bjx", []):
                if p.get("b") == "A100":
                    record.setdefault("signatur_a100", []).append(p.get("j"))
                    code = p.get("x", [])
                    if isinstance(code, str) and code.startswith("NELA100"):
                        year = "20{}".format(code[7:9])
                        record.setdefault("neuerwerbungscode", []).append(year)
        no_gnd_id = True
        for person in persons:
            id_ = person.get("id")
            self.entities[id_] = person
            if id_:
                if person.get("id") != person.get("label"):
                    no_gnd_id= False
                self.object_entity_lookup.setdefault(person.get("id") or person.get("label"), []).append(record)
        if no_gnd_id:
            record.setdefault("tag",[]).append("kein GND Eintrag bei Bib-Record")







