#!/usr/bin/python3

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="rdv_bla_data_ubit",
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="prepare bla for rdv",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ub-unibas/rdv/services/rdv_bla_data_ubit",
    install_requires=['elasticsearch<8', 'requests','digi_oai_eportal_ubit', 'mongo_decorator_ubit', 'rdv_hierarchy_ubit', 'es_ingester_ubit',
                      'pygsheets', 'rdv_marc_ubit', 'cache_decorator_redis_ubit', 'gdspreadsheets_ubit',
                      'rdv_data_helpers_ubit>0.1.9', 'rdv_entity_data_ubit', 'rdv_oaimarc_data_ubit'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #package_dir={"": "logging-decorator"},
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    include_package_data=True,
    zip_safe=False
)